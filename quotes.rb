# For this app we will use the op-uri library, which extends
# Kernel#open and helps us in accessing the web.  It accepts URIs for FTP and HTTP.
# When opened, these resources can be treateds as if they were local files,
# accessed using conventional IO methods.
# The second library is csv, and it is capable of parsing comma-separated value
# files.
require 'sinatra'
require 'open-uri'
require 'csv'

# By default error will catch Sinatra::ServerError
# Sinatra will pass you the error via the 'sinatra.error'
# in request.env
error do
  e = request.env['sinatra.error']
  puts e.to_s
  puts e.backtrace.join("\n")
  "Application Error"
end

# our class
class Stock
  def initialize(symbol)
    @symbol = symbol.upcase
    get_info
  end
  attr_reader :row0, :row1, :row2, :row3, :row3, :row4, :row5, :row6, :row7
  private # why is this private?
    # Get info about each specific stock
    # var url is like any, plain old string, but once it is parsed (think split) it becomes an array.
    def get_info
      url = "http://download.finance.yahoo.com/d/quotes.csv?s=#{@symbol}&f=sl1d1t1c1ohgv&e=.csv"
      csv = CSV.parse(open(url).read)
      csv.each do |row|
        @row0 = row[0]
        @row1 = row[1]
        @row2 = row[2]
        @row3 = row[3]
        @row4 = row[4]
        @row5 = row[5]
        @row6 = row[6]
        @row7 = row[7]
      end
    end
end
# Get new stock symbol
get '/' do
 erb :new
end

# Display stock details
get '/show' do
 @stock = Stock.new(params[:symbol])
 erb :show
end

